/* eslint-disable react/jsx-filename-extension */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';

import '../App.global.css';

import Forecast from './Forecast';

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <Forecast />
      </div>
    );
  }
}
export default App;
