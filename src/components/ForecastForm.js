import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { updateInput, updateForecastWithFetched } from '../actions/weather';
import '../App.global.css';
import Button from './Button';
import Loader from './Loader';

class ForecastForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      city: '',
    };
  }

  updateCity = (event) => {
    this.setState({
      city: event.target.value,
    });
  };

  fetchForecast = () => {
    this.props.updateForecastWithFetched(this.state.city);
  };

  render() {
    let load;
    if (!this.props.loader) {
      load = <Button label="Search" onClick={this.fetchForecast} />;
    } else {
      load = <Loader />;
    }
    return (
      <div>
        <input
          className="input"
          value={this.state.city}
          onChange={this.updateCity}
          placeholder="Choose a  city :)"
         />
        {load}
        <Loader />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    input: state.weatherReducer.input,
    loader: state.weatherReducer.loader,
  };
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      updateInput,
      updateForecastWithFetched,
    },
    dispatch
  );
}
export default connect(mapStateToProps, mapDispatchToProps)(ForecastForm);
