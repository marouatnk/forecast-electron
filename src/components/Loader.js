import React, { Component } from 'react';
import '../App.global.css';

class Loader extends Component {
  render() {
    return (
      <div>
        <image
          className="loader"
          alt="Loader"
          width="25"
          height="25"
          src="../images/loading.gif"
        />
      </div>
    );
  }
}
export default Loader;
